﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public GameObject tilePrefab;
    public Sprite[] tile;
    private int quad;
    public int[,] levelMap =
         {
         {1,2,2,2,2,2,2,2,2,2,2,2,2,7},
         {2,5,5,5,5,5,5,5,5,5,5,5,5,4},
         {2,5,3,4,4,3,5,3,4,4,4,3,5,4},
         {2,6,4,0,0,4,5,4,0,0,0,4,5,4},
         {2,5,3,4,4,3,5,3,4,4,4,3,5,3},
         {2,5,5,5,5,5,5,5,5,5,5,5,5,5},
         {2,5,3,4,4,3,5,3,3,5,3,4,4,4},
         {2,5,3,4,4,3,5,4,4,5,3,4,4,3},
         {2,5,5,5,5,5,5,4,4,5,5,5,5,4},
         {1,2,2,2,2,1,5,4,3,4,4,3,0,4},
         {0,0,0,0,0,2,5,4,3,4,4,3,0,3},
         {0,0,0,0,0,2,5,4,4,0,0,0,0,0},
         {0,0,0,0,0,2,5,4,4,0,3,4,4,4},
         {2,2,2,2,2,1,5,3,3,0,4,0,0,0},
         {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
         };

    public int[,] levelMap1 =
    {
        {0,0,0,0,0,0,5,0,0,0,4,0,0,0},
        {2,2,2,2,2,1,5,3,3,0,4,0,0,0},
        {0,0,0,0,0,2,5,4,4,0,3,4,4,0},
        {0,0,0,0,0,2,5,4,4,0,0,0,0,0},
        {0,0,0,0,0,2,5,4,3,4,4,3,0,3},
        {1,2,2,2,2,1,5,4,3,4,4,3,0,4},
        {2,5,5,5,5,5,5,4,4,5,5,5,5,4},
        {2,5,3,4,4,3,5,4,4,5,3,4,4,3},
        {2,5,3,4,4,3,5,3,3,5,3,4,4,4},
        {2,5,5,5,5,5,5,5,5,5,5,5,5,5},
        {2,5,3,4,4,3,5,3,4,4,4,3,5,3},
        {2,6,4,0,0,4,5,4,0,0,0,4,5,4},
        {2,5,3,4,4,3,5,3,4,4,4,3,5,4},
        {2,5,5,5,5,5,5,5,5,5,5,5,5,4},
        {1,2,2,2,2,2,2,2,2,2,2,2,2,7},
    };

    public int[,] levelMap2 =
    {
        {0, 0, 0, 4, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 4, 0, 3, 3, 5, 1, 2, 2, 2, 2, 2},
        {0, 4, 4, 3, 0, 4, 4, 5, 2, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 4, 4, 5, 2, 0, 0, 0, 0, 0},
        {3, 0, 3, 4, 4, 3, 4, 5, 2, 0, 0, 0, 0, 0},
        {4, 0, 3, 4, 4, 3, 4, 5, 1, 2, 2, 2, 2, 1},
        {4, 5, 5, 5, 5, 4, 4, 5, 5, 5, 5, 5, 5, 2},
        {3, 4, 4, 3, 5, 4, 4, 5, 3, 4, 4, 3, 5, 2},
        {4, 4, 4, 3, 5, 3, 3, 5, 3, 4, 4, 3, 5, 2},
        {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2},
        {3, 5, 3, 4, 4, 4, 3, 5, 3, 4, 4, 3, 5, 2},
        {4, 5, 4, 0, 0, 0, 4, 5, 4, 0, 0, 4, 6, 2},
        {4, 5, 3, 4, 4, 4, 3, 5, 3, 4, 4, 3, 5, 2},
        {4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2},
        {7, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1},
    };

    public int[,] levelMap4 = {
        {7, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1},
        {4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2},
        {4, 5, 3, 4, 4, 4, 3, 5, 3, 4, 4, 3, 5, 2},
        {4, 5, 4, 0, 0, 0, 4, 5, 4, 0, 0, 4, 6, 2},
        {3, 5, 3, 4, 4, 4, 3, 5, 3, 4, 4, 3, 5, 2},
        {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2},
        {4, 4, 4, 3, 5, 3, 3, 5, 3, 4, 4, 3, 5, 2},
        {3, 4, 4, 3, 5, 4, 4, 5, 3, 4, 4, 3, 5, 2},
        {4, 5, 5, 5, 5, 4, 4, 5, 5, 5, 5, 5, 5, 2},
        {4, 0, 3, 4, 4, 3, 4, 5, 1, 2, 2, 2, 2, 1},
        {3, 0, 3, 4, 4, 3, 4, 5, 2, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 4, 4, 5, 2, 0, 0, 0, 0, 0},
        {4, 4, 4, 3, 0, 4, 4, 5, 2, 0, 0, 0, 0, 0},
        {0, 0, 0, 4, 0, 3, 3, 5, 1, 2, 2, 2, 2, 2},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    };

    int vertical, horizontal, cols, rows;
    // Start is called before the first frame update
    void Start()
    {
        vertical = (int)Camera.main.orthographicSize;
        horizontal = vertical * (Screen.width / Screen.height);
        cols = 15;
        rows = 14;


        //quad 3
        quad = 3;
        for (int i = 0; i < cols; i++) { //int i = 0; i < cols; i++
            for (int j = 0; j < rows; j++) { //int j = 0; j < rows; j++
                if (levelMap[i, j] != 0) {
                    if (j == 0 && i < 9)                        //x = j = row, y = i
                    {
                        SpawnTile(j, i, levelMap[i, j], 90);
                    }
                    else if (j == 2 & i == 2 || j == 2 & i == 3 || j == 2 & i == 6)
                    {
                        SpawnTile(j, i, levelMap[i, j], 90);
                    }
                    else if (j == 5)
                    {
                        if (i == 2 || i == 6 || i == 9)
                        { //i==2 || 
                            SpawnTile(j, i, levelMap[i, j], 180);
                        }
                        else if (i == 3 || i == 10 || i == 11 || i == 12)
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else if (i == 4 || i == 7 || i == 13)
                        {
                            SpawnTile(j, i, levelMap[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }
                    else if (j == 7)
                    {
                        if (i == 2 || i == 3 || (i >= 6 && i <= 12))
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }
                    else if (j == 8)
                    {
                        if (i == 6)
                        {
                            SpawnTile(j, i, levelMap[i, j], 180);
                        }
                        else if (i >= 7 && i <= 12 && i != 9)
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else if (i == 13)
                        {
                            SpawnTile(j, i, levelMap[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }
                    else if (j == 10)
                    {
                        if (i == 6 || i == 12 || i == 13)
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }
                    else if (j == 11)
                    {
                        if (i == 3)
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else if (i == 2 || i == 9)
                        {
                            SpawnTile(j, i, levelMap[i, j], 180);
                        }
                        else if (i == 4 || i == 10)
                        {
                            SpawnTile(j, i, levelMap[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }
                    else if (j == 13) {
                        if ((i >= 1 && i <= 3) || i == 9 || i == 8)
                        {
                            SpawnTile(j, i, levelMap[i, j], 90);
                        }
                        else if (i == 0 || i == 7)
                        {
                            SpawnTile(j, i, levelMap[i, j], 180);
                        }
                        else {
                            SpawnTile(j, i, levelMap[i, j], 0);
                        }
                    }


                    else
                    {
                        SpawnTile(j, i, levelMap[i, j], 0);
                    }
                }

                /* 
                    if (levelMap[i, j] != 0) { 
                    SpawnTile(j, i, levelMap[i, j],0);
                }
                */
            }
        }

        
        //quad 1
        quad = 1;
        for (int j = 0; j < rows; j++)
        {
            for (int i = 0; i < cols; i++)
            { 
                if (levelMap1[i, j] != 0) {
                    if (j == 0)
                    {
                        if (i >= 5 && i <= 13)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 2)
                    {
                        if (i == 10 || i == 11 || i == 7)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 5)
                    {
                        if (i == 11 || (i >= 2 && i <= 4))
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else if (i == 12 || i == 8 || i == 5)
                        {
                            SpawnTile(j, i, levelMap1[i, j], -90);
                        }
                        else if (i == 10 || i == 7||i==1)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 7)
                    {
                        if ((i >= 1 && i <= 7) || i == 10 || i == 11)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 8)
                    {
                        if (i >= 2 && i <= 7 && i != 4)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else if (i == 8)
                        {
                            SpawnTile(j, i, levelMap1[i, j], -90);
                        }
                        else if (i == 1)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 10)
                    {
                        if (i == 0 || i == 1 || i == 7)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 11)
                    {
                        if (i == 11)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else if (i == 5 || i == 12)
                        {
                            SpawnTile(j, i, levelMap1[i, j], -90);
                        }
                        else if (i == 4 || i == 10)
                        {
                            SpawnTile(j, i, levelMap1[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else if (j == 13) {
                        if (i == 4 || i == 5 || i == 6 || (i >= 10 && i <= 13))
                        {
                            SpawnTile(j, i, levelMap1[i, j], 90);
                        }
                        else if (i == 7) {
                            SpawnTile(j, i, levelMap1[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap1[i, j], 0);
                        }
                    }
                    else
                    {
                        SpawnTile(j, i, levelMap1[i, j], 0);
                    }
                    
                }
                
                /*if (levelMap1[j, i] != 0)
                {
                    if (j == 0 & i < 9)
                    {
                        SpawnTile(i, j, levelMap1[j, i], 90);
                    }
                    Debug.Log("Spawning?");
                    SpawnTile(i, j, levelMap1[j, i], 0);
                }*/

            }
        }

       

        quad = 2;
        for (int j = 0; j < rows;j++)
        {
            for (int i = cols - 1; i >= 0; i--)
            {
                if (levelMap2[i, j] != 0)
                {
                    if (j == 13)
                    {
                        if (i > 5 && i <= 13)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i==14)
                        {
                            SpawnTile(j, i, levelMap2[i, j], -90);
                        }
                        else if (i == 5)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 11)
                    {
                        if (i == 11)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i == 8 || i == 12 )
                        {
                            SpawnTile(j, i, levelMap2[i, j], -90);
                        }
                        else if (i == 10 || i == 7)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 8)
                    {
                        if (i == 11 || (i >= 2 && i <= 4) || i == 1 || i == 7 || i == 10 )
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 6)
                    {
                        if ((i > 1 && i <= 7) || i == 11)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i == 8 || i == 12) 
                        {
                            SpawnTile(j, i, levelMap2[i, j], -90);
                        }
                        else if (i == 1 || i == 10)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 5)
                    {
                        if (i >= 2 && i <= 7 && i != 4 && i!=5 || i == 1)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i==4)
                        {
                            SpawnTile(j, i, levelMap2[i, j], -90);
                        }
                        else if (i==5)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 3)
                    {
                        if (i == 0 || i == 1)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i == 2 || i == 8)
                        {
                            SpawnTile(j, i, levelMap2[i, j], -90);
                        }
                        else if (i == 7)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 2)
                    {
                        if (i == 11||i==10||i==4)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i == 4)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else if (j == 0)
                    {
                        if (i == 5 || i == 6 || (i >= 11 && i <= 13))
                        {
                            SpawnTile(j, i, levelMap2[i, j], 90);
                        }
                        else if (i == 4||i==10)
                        {
                            SpawnTile(j, i, levelMap2[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap2[i, j], 0);
                        }
                    }
                    else
                    {
                        SpawnTile(j, i, levelMap2[i, j], 0);
                    }

                }

            }
        }


        
        //quad 4
        quad = 4;
        for (int j = 0; j < rows; j++)
        { 
            for (int i = cols-1; i >= 0 ; i--)
            { 
                if (levelMap4[i, j] != 0)
                {
                    if (j == 13)                        //x = j, y = i
                    {
                        if (i < 9 && i!=0)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 9) {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else if (i == 0)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    
                    else if (j == 11)
                    {
                        if (i == 3)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 4 || i == 7) {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else if (i == 2 || i == 6)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }

                    else if (j == 8)
                    {
                        
                        if (i == 2 || i == 3 || i == 6 || i == 9||i == 10 || i == 11 || i == 12)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    
                    else if (j == 7)
                    {
                        if (i == 6)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else if (i >= 7 && i <= 12 && i != 9)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 13)
                        {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    else if (j == 6)
                    {
                        if (i == 4 || i==13)
                        {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else if (i==2||i==6)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else if (i == 2 || i == 3 || (i >= 6 && i <= 12))
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }

                    else if (j == 5)
                    {
                        if (i == 6 || i == 7 || i == 8 || i == 11 || i == 12)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 9)
                        {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else if (i == 10)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }

                    else if (j == 3)
                    {
                        if ( i == 13)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 7)
                        {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else if (i == 12 || i == 6)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    else if (j == 2)
                    {
                        if (i == 3 || i == 2 || i == 9)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    else if (j == 0)
                    {
                        if ((i >= 1 && i <= 3) || i == 9 || i == 8 || i == 7)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 90);
                        }
                        else if (i == 0)
                        {
                            SpawnTile(j, i, levelMap4[i, j], 180);
                        }
                        else if (i == 4 || i == 10)
                        {
                            SpawnTile(j, i, levelMap4[i, j], -90);
                        }
                        else
                        {
                            SpawnTile(j, i, levelMap4[i, j], 0);
                        }
                    }
                    else
                    {
                        SpawnTile(j, i, levelMap4[i, j], 0);
                    }
                }
            }
        }
    }

    private Vector3 GridToWorld(int x, int y) {
        if (quad == 1)
        {
            return new Vector3(x - 13, y);
        }
        else if (quad == 2)
        {
            return new Vector3(x + 1, y);
        }
        else if (quad == 3)
        {
            return new Vector3(x - 13, y - 14);
        }
        else
        {
            return new Vector3(x +1, y - 14);
        }
        
        //
    }
    // Update is called once per frame
    private void SpawnTile(int x, int y, int piece, int angle)
    {
        SpriteRenderer sr = Instantiate(tilePrefab, GridToWorld(x, y), Quaternion.Euler(new Vector3(0, 0, angle))).GetComponent<SpriteRenderer>();
        sr.name = "quadrant" + quad + " x: " + x + " y: " + y;
        sr.sprite = tile[(piece - 1)];
        sr.transform.parent = transform;

        if (piece == 2 || piece == 3) {
            sr.tag = "Wall";
        }
        

        //GameObject g = new GameObject("x: " + x + "y: " + y);
        //g.transform.position = new Vector3(x - (horizontal - 0.5f), y - (vertical - 0.5f));
    }
}
