﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PacStudentController : MonoBehaviour
{
    private Vector2 lastInput = Vector2.zero;
    private float speed = 3.0f;
    [SerializeField]
    private AudioSource walk;
    private float timer;
    private int lastTime;
    // Start is called before the first frame update
    void Start()
    {
        lastTime = 0;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        GetMovementInput();
        Move();

        if (lastInput != Vector2.zero)
        {
            timer += Time.deltaTime;
            if ((int)timer == lastTime)
            {
                lastTime = (int)timer +1;
                walk.Play();
            }
        }


    }

    public void GetMovementInput()
    {
        /*if (Input.GetAxis("Vertical") != 0) {
            MoveY();
        } 
        if (Input.GetAxis("Horizontal") != 0) {
            MoveX();
        }*/

        /* movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
         movement = Vector3.ClampMagnitude(movement, 1.0f);
         movementSqrMagnitude = movement.sqrMagnitude;*/

        //Debug.Log("Movement: " + movement);

        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            lastInput = Vector2.left;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            lastInput = Vector2.right;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            lastInput = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            lastInput = Vector2.down;
        }
    }


    void Move()
    {
        transform.localPosition += (Vector3)(lastInput * speed) * Time.deltaTime;
    }

    /*void Rotate() {
        if (dir != Vector2.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(dir, Vector2.up);
            transform.rotation = rotation;
        }
    }*/
}
